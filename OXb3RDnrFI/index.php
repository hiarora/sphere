
<html>
<head>
	<script src="../assets/js/jquery-2.0.2.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css"> 
	<link rel="stylesheet" type="text/css" href="../assets/css/style.css"> 
</head>
<body>
<div class="alert alert-success" role="alert">
  <a href="#" class="alert-link"></a>
</div>
<div class="alert alert-warning" role="alert">
  <a href="#" class="alert-link"></a>
</div>
<span><img id="detail-icon-img" src="https://cdn0.iconfinder.com/data/icons/social-messaging-ui-color-shapes/128/home-circle-blue-32.png" alt="address, blue, casa, circle, home, house, local icon" width="32" height="32"/>
<h2  id="nameHeader" style="margin-left: 540px;"> 
</h2></span>

<input id="friendtoken" type="hidden" name="token" value="OXb3RDnrFI" />
<div id="addFriend" class ="addFriend">
	<div class="input-group">
		<span class="input-group-addon" id="basic-addon1">Friend Name</span>
		<input type="text" id="name" class="form-control" aria-describedby="basic-addon1">
	</div>
	<div class="input-group" style="margin-top: 10px;">
		<span class="input-group-addon" id="basic-addon1">Email ID</span>
		<input type="email" id="email" class="form-control" aria-describedby="basic-addon1">
	</div>
	<div style="margin-top: 10px; height:30px;">
		<button id="submit" class="btn-primary" style="width:124px; float: right;">Add</button>
	</div> 
	<div id="moreInfoButton" style="margin-top: 10px; height:30px;">
		<button id ="showMoreInfo" style="width:124px; float: right;" class="btn-primary">Show More Info?</button>
	</div>
</div>

<div id="moreInfo">
	<div class="input-group">
		<span class="input-group-addon additionalInfo" id="basic-addon1">Birthday</span>
		<input type="date" id="bday" class="form-control" aria-describedby="basic-addon1">
		<button id="updatebday" style="border-radius: 4px;height: 34px;margin-left: 2px;" class="btn-primary">Update</button>
	</div>
	<div class="input-group" style="margin-top: 10px;">
		<span class="input-group-addon additionalInfo" id="basic-addon1">School</span>
		<input type="text" id="school" class="form-control" aria-describedby="basic-addon1">
		<button id="updateschool" style="border-radius: 4px;height: 34px;margin-left: 2px;" class="btn-primary">Update</button>
	</div>
	<div>
	</div>
	<div class="classes">
		
	</div>
	<span><button id = "updateClasses" style="border-radius: 4px;float:right;height: 34px;margin-left: 2px;" class="btn-primary">Update Classes</button></span>	
</div>
</body>
<script>
var apiPrefixlink = "http://ec2-54-193-5-33.us-west-1.compute.amazonaws.com/sphere/API/";

function showSuccess(message){
	$(".alert-success").text(message);
	$(".alert-success").show();
	$(".alert-success").fadeOut(3500);
}
function showWarning(message){
	$(".alert-warning").text(message);
	$(".alert-warning").show();
	$(".alert-warning").fadeOut(3500);
}

$(document).on("click","#detail-icon-img", function()
{
	window.location.href = "http://ec2-54-193-5-33.us-west-1.compute.amazonaws.com/sphere/";
});
$(document).on("click","#submit", function()
{
	if($("#name").val() == "" ||  $("#email").val() == ""){
		showWarning("Name or Email fields cannot be empty");
		return;
	}
	var myapi = apiPrefixlink + "createUser.php?name=" + $("#name").val() + "&emailid=" + $("#email").val() +  "&ownerFriendId=" + $("#friendtoken").val();
	$.get(myapi, function(data) {
		showSuccess($("#name").val() + " was added as a friend of " + $("#nameHeader").text());
	});
});

$(document).on("click","#showMoreInfo", function()
{
	 $("#moreInfo").fadeToggle();
	 $("#showMoreInfo").text($("#showMoreInfo").text()=="Hide Info?"?"Show More Info?":"Hide Info?");
});

$(document).on("click","#updatebday", function()
{
	if($("#bday").val() == ""){
		showWarning("Please select a date for birthday field.");
		return;
	}
	 var myApi = apiPrefixlink + "updateBirthday.php?bday=" + $("#bday").val() + "&uniqueID=" + $("#friendtoken").val();
	 $.get(myApi, function(data) {
		showSuccess("Birthday updated successfully!");
	 });
});

$(document).on("click","#updateschool", function()
{
	if($("#school").val() == ""){
		showWarning("School field cannot be left blank!");
		return;
	}
	var myApi = apiPrefixlink + "updateSchool.php?school=" + $("#school").val() + "&uniqueID=" + $("#friendtoken").val();
	$.get(myApi, function(data) {
		showSuccess("School updated successfully");
	});
});

$(document).on("click","#updateClasses", function()
{
	var myApi = apiPrefixlink + "updateClasses.php?uniqueID=" + $("#friendtoken").val();
	var ctr = 0;
	$(".checkboxes").each(function()
	{
		if($(this).is(":checked")){
			myApi += "&code" + ++ctr + "=" + $(this).attr("code");
		}
	});
	if(ctr == 4){
		$.get(myApi, function(data) {
			showSuccess("Classes updated successfully!");
		});
	}
	else{
		showWarning("Selected classes are less or more than 4!");
	}
});
$( document ).ready(function() {
	var myApi = apiPrefixlink + "getClasses.php";
	$.get(myApi, function(data) {
		var myCheckboxString = "<div class=\"checkbox\"><label><input class=\"checkboxes\" type=\"checkbox\" code=\"";
		var myFinalString = "";
		for( i = 0; i < data.data.length; i++){
			myFinalString +=  myCheckboxString + data.data[i].code + "\">" + data.data[i].className + "</label></div>"
		}
		$(".classes").append(myFinalString);
	}).done(function(data, textStatus, jqXHR) {
			var myInfoApi = apiPrefixlink + "getAllInfo.php?uniqueID="+$("#friendtoken").val();
			$.get(myInfoApi, function(data) {
			$("#nameHeader").text(data.data[0].name);
			$("#bday").val(data.data[0].bday);
			$("#school").val(data.data[0].school);
			var res = data.data[0].classes.split("|");
			res.forEach(function (item, index, array) {
			  console.log($($(".checkboxes")[item-1]).prop( "checked", true ));
			}); 
		
		});
		});
	
})
</script>
</html>

