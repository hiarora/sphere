<html>
<head>
	<script src='assets/js/jquery-2.0.2.js' type='text/javascript'></script>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"> 
	<link rel="stylesheet" type="text/css" href="assets/css/style.css"> 
</head>
<body>
	<div class ='tableDiv' id="content">
		<div>
			<h2>Sphere php test!</h2>
		</div>
		<div id = "userlist">
		</div>
	</div>
<script>
var apiPrefixlink = "http://ec2-54-193-5-33.us-west-1.compute.amazonaws.com/sphere/API/";
var sphereLink = "http://ec2-54-193-5-33.us-west-1.compute.amazonaws.com/sphere/";
$(document).ready(function() {
	var ar=[];
	var myClassApi = apiPrefixlink + "getClasses.php";
	$.ajax({
				async: false,
				type: "GET",
				url: myClassApi,
			}
		   ).done(function(data, textStatus, jqXHR) {
				for( i = 0; i < data.data.length; i++){
						ar[data.data[i].code] = data.data[i].className;
				}
 		   });
	var myApi = apiPrefixlink + "getUsers.php";
	$.get(myApi , function(data) {
		var myTableString = "<table class='table table-hover'><tbody><tr id=\"headrow\"><th>Name</th><th>Unique Ids</th><th>Classes</th><th>Friends</th></tr>";
		for( i = 0; i < data.data.length; i++){
			var res = data.data[i].classes.split("|");
			var myClasses = "-";
			if(res.length > 1){
				 myClasses = "";
				res.forEach(function (item, index, array) {
				  myClasses += ar[item] + (index != 3?", ":"");
				});
			}
			myTableString += "<tr id=\"" + data.data[i].uniqueID + "\"><td>" + data.data[i].name + "</td><td><a href=" + sphereLink + data.data[i].uniqueID + ">" + data.data[i].uniqueID  + "</a></td><td>" + myClasses + "</td><td><button ownerid=\"" + data.data[i].uniqueID + "\" class = \" btn-primary friends\" friends=\"" + data.data[i].friends + "\">Friends</button></td></tr>";
			console.log(data.data[i].friends);
		}
		myTableString += "</tbody></table>";
		$("#userlist").append(myTableString);
	});
});


$(document).on("click",".friends", function()
{
	if($(this).text() == "Reset"){
		$(this).text("Friends");
		$("tr").show();		
		var friends = $(this).attr('friends');
		var res = friends.split("|");
		res.forEach(function (item, index, array) {
		  $("#"+item).find(".friends").show();
		});
		return;
	}
	var friends = $(this).attr('friends');
	$(this).text("Reset");
	var res = friends.split("|");
	$("tr").hide();
	$("#headrow").show();
	$("tr").children(0).show();
	$("#"+$(this).attr('ownerid')).show();
	res.forEach(function (item, index, array) {
   	  console.log($("#"+item).find(".friends").hide());
	  $("#"+item).fadeIn();
	});
});

</script>
</html>



