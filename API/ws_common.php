<?php
include_once("database_config.php");
ini_set('max_execution_time', 300);

define('MESSAGE_0','Success');
define('MESSAGE_101','No users found.');
define('MESSAGE_501','An unknown error occurred!');
class ws_common
{			
	public function GetAllUsers()
	{
		$array = array();
		$query = "SELECT * FROM `Users`";
		try
		{
			$retval = mysql_query($query);
			if(mysql_num_rows($retval) == 0)
			{
				return $this->JSONResponse(101, MESSAGE_101, $array);
			}
			while ($row = mysql_fetch_array($retval)) 
			{
				$result = new stdClass;
				$result->name =$row['Name'];
				$result->uniqueID = $row['UniqueID'];
				$result->friends = $row['Friends'];
				$result->email = $row['Email'];
				$result->classes = $row['Classes'];
				array_push($array, $result);
			}
			return $this->JSONResponse(0, MESSAGE_0, $array);
		}
		catch (Exception $ex) 
		{	
			return $this->JSONResponse(501, MESSAGE_501, $array);
		}	
	}
	
	public function CreateRandomString()
	{
		$seed = str_split('abcdefghijklmnopqrstuvwxyz'
                 .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                 .'0123456789'); // and any other characters
		shuffle($seed); // probably optional since array_is randomized; this may be redundant
		$rand = '';
		foreach (array_rand($seed, 10) as $k) $rand .= $seed[$k];
		return $rand;
	}
	
	public function CreateUser($ownerFriendId, $name, $emailid)
	{
		$array = array();
		$myRandomString = $this->CreateRandomString();
		
		//create folder
		if (!mkdir("../".$myRandomString, 0777, true)) {
			die('Failed to create folders...');
		}
		$query = "INSERT INTO `Users` (Name, UniqueID, Email, Friends) VALUES('".$name."', '".$myRandomString."', '".$emailid."', '".$ownerFriendId."')";
		mysql_query($query);
		// create index.php
		$query = "SELECT * FROM  `Template`";
		$retval = mysql_query($query);
		$row = mysql_fetch_array($retval);
		$result =  $row['Code'];
		$myNewPage = str_replace("OXb3RDnrFI", $myRandomString, $result);
		$myfile = fopen("../".$myRandomString."/index.php", "w") or die("Unable to open file!");
		fwrite($myfile, $myNewPage);
		fclose($myfile);	
		//make friends with owner
		$query = "SELECT Friends from `Users` where UniqueID = '".$ownerFriendId."'";
		$retval = mysql_query($query);
		if(mysql_num_rows($retval) == 0)
		{
			return $this->JSONResponse(101, MESSAGE_101, $array);
		}
		$row = mysql_fetch_array($retval);
		$result =  $row['Friends'];
		if($result == "")
			$result = $myRandomString;
		else 
			$result = $result."|".$myRandomString;
		$query = "UPDATE `Users` SET Friends='". $result."' where UniqueID = '".$ownerFriendId."'";
		mysql_query($query);
	}
		
	public function UpdateBirthday($uniqueID, $bday)
	{
		
		try
		{
			$query = "UPDATE `Users` SET Bday='".$bday."' where UniqueID = '".$uniqueID."'";
			mysql_query($query);
			return $this->JSONResponse(0, MESSAGE_0, $array);
		}
		catch (Exception $ex) 
		{	
			return $this->JSONResponse(501, MESSAGE_501, $array);
		}
	}
	
	public function UpdateSchool($uniqueID, $school)
	{
		try
		{
			$query = "UPDATE `Users` SET School='".$school."' where UniqueID = '".$uniqueID."'";
			mysql_query($query);
			return $this->JSONResponse(0, MESSAGE_0, $array);
		}
		catch (Exception $ex) 
		{	
			return $this->JSONResponse(501, MESSAGE_501, $array);
		}
	}
	public function GetAllClasses()
	{
		$array = array();
		$query = "SELECT * FROM `Classes`";
		try
		{
			$retval = mysql_query($query);
			if(mysql_num_rows($retval) == 0)
			{
				return $this->JSONResponse(101, MESSAGE_101, $array);
			}
			while ($row = mysql_fetch_array($retval)) 
			{
				$result = new stdClass;
				$result->code =$row['Code'];
				$result->className = $row['Class'];
				array_push($array, $result);
			}
			return $this->JSONResponse(0, MESSAGE_0, $array);
		}
		catch (Exception $ex) 
		{	
			return $this->JSONResponse(501, MESSAGE_501, $array);
		}	
	}
	
	public function UpdateClasses($uniqueID, $code1, $code2, $code3, $code4)
	{
		try
		{
			$query = "UPDATE `Users` SET Classes='".$code1."|".$code2."|".$code3."|".$code4."' where UniqueID = '".$uniqueID."'";
			mysql_query($query);
			return $this->JSONResponse(0, MESSAGE_0, $array);
		}
		catch (Exception $ex) 
		{	
			return $this->JSONResponse(501, MESSAGE_501, $array);
		}
	}
	
	public function GetAllInfo($uniqueID)
	{
		$array = array();
		$query = "SELECT * FROM `Users` where UniqueID = '".$uniqueID."'";
		try
		{
			$retval = mysql_query($query);
			if(mysql_num_rows($retval) == 0)
			{
				return $this->JSONResponse(101, MESSAGE_101, $array);
			}
			while ($row = mysql_fetch_array($retval)) 
			{
				$result = new stdClass;
				$result->name =$row['Name'];
				$result->bday =$row['Bday'];
				$result->school = $row['School'];
				$result->classes = $row['Classes'];
				array_push($array, $result);
			}
			return $this->JSONResponse(0, MESSAGE_0, $array);
		}
		catch (Exception $ex) 
		{	
			return $this->JSONResponse(501, MESSAGE_501, $array);
		}
	}
	
	public function JSONResponse($code, $message, $data)
	{
		$result = new stdClass;
		$result->code = $code;
		$result->message = $message;
		$result->data = $data;		
		return json_encode($result);
	}
}
?>