-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 20, 2015 at 09:57 PM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `Sphere`
--

-- --------------------------------------------------------

--
-- Table structure for table `Classes`
--

CREATE TABLE IF NOT EXISTS `Classes` (
  `Code` int(11) NOT NULL AUTO_INCREMENT,
  `Class` varchar(200) NOT NULL,
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `Classes`
--

INSERT INTO `Classes` (`Code`, `Class`) VALUES
(1, 'Artificial Intelligence'),
(2, 'Databases'),
(3, 'Data Mining'),
(4, 'Robotics'),
(5, 'Statistic'),
(6, 'Probability'),
(7, 'Intro to programming'),
(8, 'Operating Systems');

-- --------------------------------------------------------

--
-- Table structure for table `Template`
--

CREATE TABLE IF NOT EXISTS `Template` (
  `Code` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Template`
--

INSERT INTO `Template` (`Code`) VALUES
('\r\n<html>\r\n<head>\r\n	<script src="../assets/js/jquery-2.0.2.js" type="text/javascript"></script>\r\n	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css"> \r\n	<link rel="stylesheet" type="text/css" href="../assets/css/style.css"> \r\n</head>\r\n<body>\r\n<div class="alert alert-success" role="alert">\r\n  <a href="#" class="alert-link"></a>\r\n</div>\r\n<div class="alert alert-warning" role="alert">\r\n  <a href="#" class="alert-link"></a>\r\n</div>\r\n<h2 style="margin-left: 38%;" id="nameHeader"> \r\n</h2>\r\n<div id="addFriend" class ="addFriend">\r\n	<div class="input-group">\r\n		<span class="input-group-addon" id="basic-addon1">Friend Name</span>\r\n		<input type="text" id="name" class="form-control" aria-describedby="basic-addon1">\r\n	</div>\r\n	<div class="input-group" style="margin-top: 10px;">\r\n		<span class="input-group-addon" id="basic-addon1">Email ID</span>\r\n		<input type="email" id="email" class="form-control" aria-describedby="basic-addon1">\r\n	</div>\r\n	<input id="friendtoken" type="hidden" name="token" value="OXb3RDnrFI" />\r\n	<button id="submit" class="btn-primary" style="float: right;">Add</button>\r\n	<div id="moreInfoButton">\r\n		<button id ="showMoreInfo" class="btn-primary">Show More Info?</button>\r\n	</div>\r\n</div>\r\n\r\n<div id="moreInfo">\r\n	<div class="input-group">\r\n		<span class="input-group-addon" id="basic-addon1">Birthday</span>\r\n		<input type="date" id="bday" class="form-control" aria-describedby="basic-addon1">\r\n		<button id="updatebday" style="border-radius: 4px;height: 34px;margin-left: 2px;" class="btn-primary">Update</button>\r\n	</div>\r\n	<div class="input-group" style="margin-top: 10px;">\r\n		<span class="input-group-addon" id="basic-addon1">School</span>\r\n		<input type="text" id="school" class="form-control" aria-describedby="basic-addon1">\r\n		<button id="updateschool" style="border-radius: 4px;height: 34px;margin-left: 2px;" class="btn-primary">Update</button>\r\n	</div>\r\n	<div>\r\n	</div>\r\n	<div class="classes">\r\n		\r\n	</div>\r\n	<span><button id = "updateClasses" style="border-radius: 4px;float:right;height: 34px;margin-left: 2px;" class="btn-primary">Update Classes</button></span>	\r\n</div>\r\n</body>\r\n<script>\r\nvar apiPrefixlink = "http://ec2-54-193-5-33.us-west-1.compute.amazonaws.com/sphere/API/";\r\n\r\nfunction showSuccess(message){\r\n	$(".alert-success").text(message);\r\n	$(".alert-success").show();\r\n	$(".alert-success").fadeOut(3500);\r\n}\r\nfunction showWarning(message){\r\n	$(".alert-warning").text(message);\r\n	$(".alert-warning").show();\r\n	$(".alert-warning").fadeOut(3500);\r\n}\r\n\r\n$(document).on("click","#submit", function()\r\n{\r\n	if($("#name").val() == "" ||  $("#email").val() == ""){\r\n		showWarning("Name or Email fields cannot be empty");\r\n		return;\r\n	}\r\n	var myapi = apiPrefixlink + "createUser.php?name=" + $("#name").val() + "&emailid=" + $("#email").val() +  "&ownerFriendId=" + $("#friendtoken").val();\r\n	$.get(myapi, function(data) {\r\n		showSuccess($("#name").val() + " was added as a friend of " + $("#nameHeader").text());\r\n	});\r\n});\r\n\r\n$(document).on("click","#showMoreInfo", function()\r\n{\r\n	 $("#moreInfo").fadeToggle();\r\n	 $("#showMoreInfo").text($("#showMoreInfo").text()=="HideInfo?"?"Show More Info?":"HideInfo?");\r\n});\r\n\r\n$(document).on("click","#updatebday", function()\r\n{\r\n	if($("#bday").val() == ""){\r\n		showWarning("Please select a date for birthday field.");\r\n		return;\r\n	}\r\n	 var myApi = apiPrefixlink + "updateBirthday.php?bday=" + $("#bday").val() + "&uniqueID=" + $("#friendtoken").val();\r\n	 $.get(myApi, function(data) {\r\n		showSuccess("Birthday updated successfully!");\r\n	 });\r\n});\r\n\r\n$(document).on("click","#updateschool", function()\r\n{\r\n	if($("#school").val() == ""){\r\n		showWarning("School field cannot be left blank!");\r\n		return;\r\n	}\r\n	var myApi = apiPrefixlink + "updateSchool.php?school=" + $("#school").val() + "&uniqueID=" + $("#friendtoken").val();\r\n	$.get(myApi, function(data) {\r\n		showSuccess("School updated successfully");\r\n	});\r\n});\r\n\r\n$(document).on("click","#updateClasses", function()\r\n{\r\n	var myApi = apiPrefixlink + "updateClasses.php?uniqueID=" + $("#friendtoken").val();\r\n	var ctr = 0;\r\n	$(".checkboxes").each(function()\r\n	{\r\n		if($(this).is(":checked")){\r\n			myApi += "&code" + ++ctr + "=" + $(this).attr("code");\r\n		}\r\n	});\r\n	if(ctr == 4){\r\n		$.get(myApi, function(data) {\r\n			showSuccess("Classes updated successfully!");\r\n		});\r\n	}\r\n	else{\r\n		showWarning("Selected classes are less or more than 4!");\r\n	}\r\n});\r\n$( document ).ready(function() {\r\n	var myApi = apiPrefixlink + "getClasses.php";\r\n	$.get(myApi, function(data) {\r\n		var myCheckboxString = "<div class=\\"checkbox\\"><label><input class=\\"checkboxes\\" type=\\"checkbox\\" code=\\"";\r\n		var myFinalString = "";\r\n		for( i = 0; i < data.data.length; i++){\r\n			myFinalString +=  myCheckboxString + data.data[i].code + "\\">" + data.data[i].className + "</label></div>"\r\n		}\r\n		$(".classes").append(myFinalString);\r\n	}).done(function(data, textStatus, jqXHR) {\r\n			var myInfoApi = apiPrefixlink + "getAllInfo.php?uniqueID="+$("#friendtoken").val();\r\n			$.get(myInfoApi, function(data) {\r\n			$("#nameHeader").text(data.data[0].name);\r\n			$("#bday").val(data.data[0].bday);\r\n			$("#school").val(data.data[0].school);\r\n			var res = data.data[0].classes.split("|");\r\n			res.forEach(function (item, index, array) {\r\n			  console.log($($(".checkboxes")[item-1]).prop( "checked", true ));\r\n			});\r\n		\r\n		});\r\n		});\r\n	\r\n})\r\n</script>\r\n</html>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE IF NOT EXISTS `Users` (
  `Name` varchar(100) NOT NULL,
  `UniqueID` varchar(100) NOT NULL,
  `Email` varchar(300) NOT NULL,
  `School` varchar(200) NOT NULL,
  `Classes` varchar(200) NOT NULL,
  `Friends` varchar(1000) NOT NULL,
  `Bday` varchar(15) NOT NULL,
  PRIMARY KEY (`UniqueID`),
  UNIQUE KEY `UniqueID` (`UniqueID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`Name`, `UniqueID`, `Email`, `School`, `Classes`, `Friends`, `Bday`) VALUES
('Ben Hauser', 'a7uE50WmYL', 'bhauser@bh.com', '', '', 'Jfh7xDRFlZ', ''),
('Girija Agarwala', 'Jfh7xDRFlZ', 'ga@ga.com', 'ASU', '2|3|4|5', 'OXb3RDnrFI|a7uE50WmYL', '1990-03-23'),
('Himanshu Arora', 'OXb3RDnrFI', 'himanshuarora.1907@gmail.com', 'UCSD', '4|5|7|8', 'Jfh7xDRFlZ', '2015-10-31');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
